require "guard/guard"

module ::Guard
  class Precompile < ::Guard::Guard
    def run_on_change(path)
      puts "Precompiling assets for test env ...."
      puts `bundle exec rake assets:precompile RAILS_ENV=test`
    end
  end
end
